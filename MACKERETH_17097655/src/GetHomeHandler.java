import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * This class acts as a HttpHandler and creates a homepage for users using the restful webservice.
 * 
@author Linton Mackereth - MMU ID 17097655
@version 1.0
@return Response 200
@throws IOException
@Date 01/12/2017

 */
public class GetHomeHandler implements HttpHandler {
	
	@Override
	public void handle(HttpExchange he) throws IOException {
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
		he.sendResponseHeaders(200, 0);
		out.write("Welcome to the RESTful Employee web Service! \n");
		out.write("Get allEmployees JSON use /get-json \n");
		out.write("Post an Employee as JSON use /post-json \n");
		out.write("Get an employee use /show-employee/?id=  e.g. /show-employee/?id=2 \n");
		out.write("Update an employee use /process-update/ \n");
		out.write("Delete an employee use /delete/ \n");
		out.close();
	}
}
