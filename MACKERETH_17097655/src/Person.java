public class Person {

	/**
	 * This class acts as a blue print to detail a Person Object.
	 * 
	@author Linton Mackereth - MMU ID 17097655
	@version 1.0
	@param name, gender, dob, address, postcode
	@Date 01/12/2017

	 */
	
	//Private variables created and set to null
	
	private String name;
    private String gender;
    private String dob;
    private String address;
    private String postcode;
    
    //Person object constructor, displays variables of what makes a Person in the Class
    public Person(String name, String gender, String dob, String address, String postcode) {
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.postcode = postcode;
    }
    
    //Getters and Setter for Person Variables

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
    
}
