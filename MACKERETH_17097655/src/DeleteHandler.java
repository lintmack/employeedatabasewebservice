import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class DeleteHandler implements HttpHandler {
	/**
	 * This httphandler allows a user to delete an employee over REST.
	 *  This can only be run once the server is running.
	 * 
	@author Linton Mackereth - MMU ID:17097655
	@version 1.0
	@param String id
	@throws IOException
	@Date 01/12/2017

	 */

	public void handle(HttpExchange he) throws IOException {
		
	BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
	EmployeeDAO dao = new EmployeeDAO();
	String id = "6";

	try {
		//calls the delete employee method and passes the id in as a String
		dao.deleteEmployee(id);
		System.out.println("Employee Deleted");
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
		//returns a 200 ok if succcessful.
	he.sendResponseHeaders(200, 0);
	System.out.println("Employee Deleted");
	out.close();
}

}
