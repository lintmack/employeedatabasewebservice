import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class EmployeeDAO {
	
	/**
	 * This class defines all CRUD methods for the Employee Database.
	 * getConnection(), getAllEmployees(), getEnployee(), insertEmployee(), updateEmployee() & deleteEmployee()
	 * 
	@author Linton Mackereth MMU ID 17097655
	@version 1.0
	@param varies depending upon the method that's called - See JavaDoc for each method.
	@return varies depending upon the method that's called - See JavaDoc for each method.
	@throws varies depending upon the method that's called - See JavaDoc for each method.
	@Date 01/12/2017

	 */

	// GET CONNECTION METHOD TO SQLite Database

	public Connection getDBConnection() {
		
		/**
		 * This method creates a new connection to the employee SQLite database.
		 * 
		@author Linton Mackereth
		@version 1.0
		@return Connection
		@Date 01/12/2017

		 */

		// An instance of connection is created called dbConnection and is set to null.
		Connection dbConnection = null;
		try {
			//Tries to use the JDBC SQlite Driver
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			
			//Catches Class Not found exception
			System.out.println(e.getMessage());
		}
		try {
			//Tries to create a new String with the url for the SQLite database.
			String dbURL = "jdbc:sqlite:emp.sqlite";
			
			//Creates a new instance of connection and passes in the database URL as a parameter.
			dbConnection = DriverManager.getConnection(dbURL);
			
			//Auto Commit set to on
			dbConnection.setAutoCommit(true);
			
			//returns the connection
			return dbConnection;
		} catch (SQLException e) {
			// catches SQLException
			System.out.println(e.getMessage());
		}
		return dbConnection;
		
		// returns a connection to the database.
	}

	// Method returns a List of all Employees within the database as an arrayList.
	
	public ArrayList<Employee> getAllEmployees() throws SQLException {
		
		/**
		 * This method returns all the employees in the database as an arrayList.
		 * 
		@author Linton Mackereth
		@version 1.0
		@return arrayList allEmployees
		@throws SQLException
		@Date 01/12/2017

		 */
		
		//Variables created and set to null
		Connection dbConnection = null;
		Statement statement = null;
		ResultSet rs = null;
		
		//String created and SQL query stored in it
		String query = "SELECT * FROM emp";
		
		//New Employee object created and set to null
		Employee temp = null;
		ArrayList<Employee> allEmployees = new ArrayList<>();
		//Creates a new Array List

		try {
			//tries to get connection to database.
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(query); // print out query
			// execute SQL query
			rs = statement.executeQuery(query);
			while (rs.next()) {
				
				temp = new Employee(rs.getString("Name"), rs.getString("Gender"), rs.getString("DOB"),
						rs.getString("Address"), rs.getString("Postcode"), rs.getInt("EmployeeNumber"),
						rs.getString("Department"), rs.getString("StartDate"), rs.getFloat("Salary"),
						rs.getString("Email"));
				allEmployees.add(temp);
			}
			System.out.println("Get All Employee Records from the Database Query Completed");

			return allEmployees;
			
			//Returns all employees

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			
			// catches SQL Exception
			return null;
		} finally {
			
			//Tries to close connections to the ResultSet, Statement and Connection.
			//Catches SQLExceptions if these fail.
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				System.out.println("Error closing results set" + e.getMessage());
			}
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				System.out.println("Error closing statment" + e.getMessage());
			}
			try {
				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				System.out.println("Error closing connection" + e.getMessage());
			}
		}
	}
	
	
	
	// GET A SINGLE EMPLOYEE RECORD 

	public Employee getEmployee(String id) throws SQLException {
		
		/**
		 * This method returns a single employee from the database as an employee object that is converted
		 * to a String and printed in the console.
		 * 
		@author Linton Mackereth
		@version 1.0
		@return emp
		@param String id
		@throws SQLException
		@Date 01/12/2017

		 */
		
		// A new Employee object is created and set to null.
		
		Employee emp = null;
		
		//String is created to store the SQL query thats SELECTS all the employee fields from the database where the ID is equal to a specific employee number.
		String query = "SELECT Name, Gender, DOB, Address, Postcode, EmployeeNumber, Department, StartDate, Salary, Email "
				+ "FROM emp WHERE EmployeeNumber = ?";
		
		
		// Method tries to get a connection with the SQLite database.
		try (Connection dbConnection = getDBConnection();
				PreparedStatement preparedStatement = dbConnection.prepareStatement(query);) {

			System.out.println("Getting employee from database\n");
			
			//Prepared Statement passes in the String Employee ID.
			
			preparedStatement.setString(1, id);

			// A new instance of Result Set is created and the prepared statement is executed.
			
			ResultSet rs = preparedStatement.executeQuery();

			//Variables are created to store the Result Set data.
			while (rs.next()) {
				String name = rs.getString("Name");
				String gender = rs.getString("Gender");
				String dob = rs.getString("DOB");
				String address = rs.getString("Address");
				String postcode = rs.getString("Postcode");
				int employeeNumber = rs.getInt("EmployeeNumber");
				String department = rs.getString("Department");
				String startDate = rs.getString("StartDate");
				Float salary = rs.getFloat("Salary");
				String email = rs.getString("Email");
				
				//Creates an Employee object from the variables above.
				
				emp = new Employee(name, gender, dob, address, postcode, employeeNumber, department, startDate, salary,
						email);
				
				//Converts the Employee object into a String
				
				System.out.println(emp.toString());
			}
			System.out.println("System retrieved the employee");
		} catch (Exception e) {
			
			// Catches and prints exception if query fails.
			
			e.printStackTrace();
		}
		
		// returns an Employee object.
		
		return emp;
	}
	
	
	
	
	
	
	

	// INSERT EMPLOYEE METHOD

	public Boolean insertEmployee(Employee emp) throws SQLException {
		
		/**
		 * This method inserts a single employee object into the database.
		 * 
		@author Linton Mackereth
		@version 1.0
		@param Employee Object
		@return Boolean
		@throws SQLException
		@Date 01/12/2017

		 */

		// An int is created and set to 0, which sets the Boolean value to 0 initially.

		int rowsAffected = 0;

		// The method tries create a connection to the SQLite database.

		try (Connection dbConnection = getDBConnection(); Statement statement = dbConnection.createStatement();) {

			System.out.println("Inserting an employee into database\n");

			// A string is created to store the Update SQL query.

			String query = "INSERT INTO emp"
					+ "(EmployeeNumber, Name, Gender, DOB, Address, Postcode, Department, StartDate, Salary, Email) VALUES"
					+ "(?, ? , ?, ?, ?, ?, ?, ?, ?, ?)";

			// A new prepared statement is created and the String with the SQL query is
			// passed as a parameter.

			PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

			// The getters that create an employee object are called to pass the parameters
			// set by the user in as an employee object.

			preparedStatement.setInt(1, emp.getEmployeeNumber());
			preparedStatement.setString(2, emp.getName());
			preparedStatement.setString(3, emp.getGender());
			preparedStatement.setString(4, emp.getDob());
			preparedStatement.setString(5, emp.getAddress());
			preparedStatement.setString(6, emp.getPostcode());
			preparedStatement.setString(7, emp.getDepartment());
			preparedStatement.setString(8, emp.getStartDate());
			preparedStatement.setFloat(9, emp.getSalary());
			preparedStatement.setString(10, emp.getEmail());

			// update is executed and rowsAffected is changed from 0 to 1.

			rowsAffected = preparedStatement.executeUpdate();

			// System prints the query has completed.

			System.out.println("Inserted into database");

			// System catches and prints SQL Exception if the query fails to run.

		} catch (Exception e) {
			e.printStackTrace();
		}

		// If the query has run successfully the value of rowsAffected will be changed
		// to 1,
		// which changes the boolean from null and returns true.
		
		return rowsAffected > 0;
	}
	
	
	
	
	
	
	

	// UPDATE EMPLOYEE METHOD

	public Boolean updateEmployee(Employee emp) throws SQLException {
		/**
		 * This method updates a single employee in the database.
		 * 
		@author Linton Mackereth
		@version 1.0
		@param Employee Object
		@return Boolean
		@throws SQLException
		@Date 01/12/2017

		 */

		//// An int is created and set to 0, which sets the Boolean value to 0
		//// initially.
		int rowsAffected = 0;

		// The method tries create a connection to the SQLite database.
		try (Connection dbConnection = getDBConnection(); Statement statement = dbConnection.createStatement();) {

			System.out.println("Updating an employee into database\n");

			// A string is created to store the Update SQL query.
			String query = "UPDATE emp SET Name = ?, Gender = ?, DOB = ?, Address = ?, Postcode = ?,"
					+ " Department = ? , StartDate = ?, Salary = ?, Email = ? " + "WHERE EmployeeNumber = ?";

			// A new prepared statement is created and the String with the SQL query is
			// passed as a parameter.

			PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

			// The getters that create an employee object are called to pass the parameters
			// set by the user in as an employee object.
			preparedStatement.setString(1, emp.getName());
			preparedStatement.setString(2, emp.getGender());
			preparedStatement.setString(3, emp.getDob());
			preparedStatement.setString(4, emp.getAddress());
			preparedStatement.setString(5, emp.getPostcode());
			preparedStatement.setString(6, emp.getDepartment());
			preparedStatement.setString(7, emp.getStartDate());
			preparedStatement.setFloat(8, emp.getSalary());
			preparedStatement.setString(9, emp.getEmail());
			preparedStatement.setInt(10, emp.getEmployeeNumber());

			// update is executed and rowsAffected is changed from 0 to 1.
			rowsAffected = preparedStatement.executeUpdate();

			// System prints the query has completed.
			System.out.println("Employee Updated");
			// System catches and prints SQL Exception if the query fails to run.
		} catch (Exception e) {
			e.printStackTrace();
		}

		// If the query has run successfully the value of rowsAffected will be changed
		// to 1,
		// which changes the boolean from null and returns true.

		return rowsAffected > 0;
	}
	
	
	
	
	
	

	// DELTE EMPLOYEE METHOD

	public Boolean deleteEmployee(String id) throws SQLException {
		
		/**
		 * This method deletes a single employee from the database.
		 * 
		@author Linton Mackereth
		@version 1.0
		@param String id
		@return Boolean
		@throws SQLException
		@Date 01/12/2017

		 */

		// An int is created and set to 0, which sets the Boolean value to 0 initially.
		int rowsAffected = 0;

		// A string is created to store the Delete SQL query.
		String query = "DELETE FROM emp WHERE EmployeeNumber = ?";

		// The method tries create a connection to the SQLite database.
		try (Connection dbConnection = getDBConnection();
				// A new prepared statement is created and the String with the SQL query is
				// passed as a parameter.
				PreparedStatement preparedStatement = dbConnection.prepareStatement(query);) {

			System.out.println("Getting employee from database\n");

			// String id is passed in as a parameter into the preparedStatement.
			preparedStatement.setString(1, id);

			// update is executed and rowsAffected is changed from 0 to 1.
			rowsAffected = preparedStatement.executeUpdate();

			System.out.println("System removed the employee");
			// System prints the query has completed.
		} catch (SQLException e) {
			// System catches and prints SQL Exception if the query fails to run.
			e.printStackTrace();
		}
		// If the query has run successfully the value of rowsAffected will be changed
		// to 1,
		// which changes the boolean from null and returns true.
		return rowsAffected > 0;
	}
}
