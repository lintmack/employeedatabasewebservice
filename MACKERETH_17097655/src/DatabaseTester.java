import java.sql.SQLException;
import java.util.ArrayList;
import com.google.gson.Gson;


public class DatabaseTester {
	
	/**
	 * This class acts as a database tester class and checks the below CRUD operations
	 * within the console.
	 * 
	@author Linton Mackereth MMU ID 17097655
	@version 1.0
	@param varies depending upon the method that's called - See JavaDoc for each method.
	@return varies depending upon the method that's called - See JavaDoc for each method.
	@throws SQLException
	@Date 01/12/2017

	 */
	
	public static void main(String[] args) throws SQLException {
		
		//Creates a new EmployeeDAO object called dao
		EmployeeDAO Dao = new EmployeeDAO();
		
		//creates a new Employee object and sets it to null
		Employee employee = null;
		
		//creates a new employee arraylist
		ArrayList<Employee> allEmployees = new ArrayList<>();
		
		//creates a new GSON object
		Gson gson = new Gson();
		
		
		
		//GetALlEmployees
		try {
			allEmployees = Dao.getAllEmployees();
			// tries to call the getAllEmployee method within the EmployeeDAO Class
		} catch (SQLException e) {
			//Catches SQLException
			e.printStackTrace();
		}
		String myJson = gson.toJson(allEmployees);
		//Creates a new String and converts allEmployees into a JSON
		System.out.println(myJson);
		
		
		
		//Get Employee
		try {
			employee = Dao.getEmployee("3");
			//tries to call the getEmployee Method and passes in a String of the Value "3"
			// String corresponds to employee id.
		} catch (SQLException e) { 
			e.printStackTrace();
		}
		
		String sue = gson.toJson(employee);
		//Creates new string and passes in employee object to convert into JSON.
		System.out.println(sue);		

		
		
		
		
		//Insert Employee
		Boolean hasInserted = null;
		//Boolean is created and set to null.
		try {
			//Creates a new employee object with parameters.
			employee = new Employee("Linton", "Questionable", "25/11/2017", " 12 Pensitone road, Scunthrope", "SCU NTS", 6,
					"Internal Affairs", "23/20/2017", 9000, "linton@hounsinghelp.com");
			
			//Tries to call insertEmployee Method and pass in the employee object from above.
			hasInserted = Dao.insertEmployee(employee);			
		} catch (SQLException e) { 
			e.printStackTrace();
		}
		System.out.println(employee);
		//Prints out employee object and confirms it has been added to the database.
		System.out.println("Inserted: " + hasInserted.toString());
		System.out.println(Dao.getAllEmployees());
		
		
		
		
		//UPDATE EMPLOYEE
		
				Boolean hasUpdated = null;
				// Boolean is created and set to null.
				try {
					//Tries to create a new employee object with parameters set below.
					employee = new Employee("Linton", "M", "02/06/1989", " 12 Pensitone road, Scunthrope", "M1 1ER", 6,
							"Internal Affairs", "23/20/2017", 9000, "linton@update.com");
					
					//calls the updateEmployee method and passes in the parameters below
					hasUpdated = Dao.updateEmployee(employee);			
				} catch (SQLException e) { 
					e.printStackTrace();
				}
				//Prints out employee object and confirms it has been added to the database.
				System.out.println("Updated: " + hasUpdated.toString());
				System.out.println(Dao.getAllEmployees());
		
		
		//Delete Employee
		
		Boolean hasDeleted = null;
		// Boolean is created and set to null.
		try {			
			//Calls the delete employee method and passes in the parameter, which is a String
			// that relates to the employee id.
			hasDeleted = Dao.deleteEmployee("6");			
		} catch (SQLException e) { 
			e.printStackTrace();
		}
		//Confirms employee has been deleted and prints out all remaining employees.
		System.out.println("Deleted: " + hasDeleted.toString());
		System.out.println(Dao.getAllEmployees());
	}	
} 

