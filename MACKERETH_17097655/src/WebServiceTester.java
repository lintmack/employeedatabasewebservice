
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Map;

import com.google.gson.Gson;


/**
 * This class acts as a webservice tester class and checks the below CRUD operations
 * work over a restful route. This can only be run once the server is running.
 * 
@author Linton Mackereth - MMU ID:17097655
@version 1.0
@param varies depending upon the method that's called - See JavaDoc for each method.
@return varies depending upon the method that's called - See JavaDoc for each method.
@throws varies depending upon the method that's called - See JavaDoc for each method.
@Date 01/12/2017

 */
public class WebServiceTester {
	
	
	public static void main(String args[]) {
		System.out.println("Employee = " + getEmployees());
	} 
	
	private static StringBuffer getEmployees() {
		
		StringBuffer response = new StringBuffer();
		
		try {
			//Creates a new URL http://localhost:8014/get-json
			URL url = new URL("http://localhost:8014/get-json");
			//Creates a new BufferedReader 
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String output;
			//Enters a while loop that runs whilst the program is able to read the next line.
			while((output = reader.readLine()) != null) {
				response.append(output);
			}
			reader.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		//returns repsonse.
		return response;	
	}
}
