
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
	
/**
 * This class acts as a handler and allows the user to post a JSON object into the database.
 * The JSON object is converted into a Java Object using GSON.
 * 
 @author Linton Mackereth
@version 1.0
@param JSON object
@return employee object in SQLite database.
@throws  IOException
@Date 01/12/2017

 */
	public class PostJsonHandler implements HttpHandler{

		

		@Override
		public void handle(HttpExchange he) throws IOException {

			final String requestMethod = he.getRequestMethod().toUpperCase();
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));

			if (!requestMethod.equals("POST")) {
				he.sendResponseHeaders(404, 0);
				out.write("Only POST is supported on this route");
				out.close();
				return;
			}
			
			//Creates a new instance of GSON
			Gson gson = new Gson();
			
			Reader inputStreamReader = new InputStreamReader(he.getRequestBody());
			
	        //Creates new Employee object called emp that converts JSON to a java employee object.

			Employee emp = gson.fromJson(inputStreamReader, Employee.class);
			System.out.println(emp);
			//Creates a new instance of EmployeeDAO called dao. 
			EmployeeDAO dao = new EmployeeDAO();
			try {
				// calls the insert into database method.
				boolean created = dao.insertEmployee(emp);
				if (created) {
					he.sendResponseHeaders(HttpURLConnection.HTTP_CREATED, 0);
					out.write("Employee Created");
				} else {
					//returns a 404 if unsuccessful.
					he.sendResponseHeaders(404, 0);
					out.write("Employee Not Created");
				}
				//catches error
			} catch (SQLException e) {
				he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
				out.write("Not OK:" + e.getMessage());
			}

			out.close();
		}
	}
