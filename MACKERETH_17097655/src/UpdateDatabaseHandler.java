import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.net.HttpURLConnection;
import java.sql.SQLException;
import java.util.Map;


/**
 * This class acts as a Handler to Update an Employee record over a RESTful route.
 * 
@author Linton Mackereth - MMU ID: 17097655
@version 1.0
@param String Query
@return result
@throws IOException
@Date 01/12/2017
 */
public class UpdateDatabaseHandler implements HttpHandler {
	

    @Override
    public void handle(HttpExchange he) throws IOException {

        final String requestMethod = he.getRequestMethod().toUpperCase();
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));

        if (!requestMethod.equals("POST")) {
            he.sendResponseHeaders(404, 0);
            out.write("Only POST is supported on this route");
            out.close();
            return;
        }

        //Creates new GSON Object
        Gson gson = new Gson();

        Reader inputStreamReader = new InputStreamReader(he.getRequestBody());
        
        //Creates new Employee object called emp that converts JSON to a java employee object.
        Employee emp = gson.fromJson(inputStreamReader, Employee.class);
        System.out.println(emp);

        EmployeeDAO dao = new EmployeeDAO();
        try {
        		//tries to pass the updated employee object into the database.
            boolean updated = dao.updateEmployee(emp);
            if (updated) {
            		//if successful 200 is returned.
                he.sendResponseHeaders(200, 0);
                out.write("Employee Updated");
            } else {
            		//if not successful 404 is returned.
                he.sendResponseHeaders(404, 0);
                out.write("Employee Not Updated");
            }
        } catch (SQLException e) {
            he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            out.write("Not OK:" + e.getMessage());
        }

        out.close();
    }
}

