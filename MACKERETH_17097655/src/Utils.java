import java.util.HashMap;
import java.util.Map;


/**
 * This class acts as a HashMap Utility class that other methods can call on.
 * 
@author Linton Mackereth - MMU ID: 17097655
@version 1.0
@param String Query
@return result
@Date 01/12/2017

 */
public class Utils {
	
    public static Map<String, String> queryToMap(String query){
    	//Creates a new HashMap called result.
        Map<String, String> result = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length>1) {
                result.put(pair[0], pair[1]);
            }else{
                result.put(pair[0], "");
            }
        }
        return result;
    }
}
