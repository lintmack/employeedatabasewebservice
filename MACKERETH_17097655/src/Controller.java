import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class Controller {

	public static void main(String[] args) {
		
		/**
		 * This class acts as controller class an initiates the HTTPServer that runs on port 8014.
		 * The class also calls the created HttpHandlers and allow the user to run the CRUD methods over
		 * the restful web service.
		 * 
		@author Linton Mackereth MMU ID 17097655
		@version 1.0
		@param varies depending upon the method that's called - See JavaDoc for each method.
		@return Connection on Port 8014 and varies methods depending upon the method that's called - See JavaDoc for each method.
		@throws IOException
		@Date 01/12/2017

		 */
				
		try {
			HttpServer server = HttpServer.create(new InetSocketAddress(8014), 0);
			
//			server.createContext("/process", new InsertDatabseHandler());
			
			//this is set up as /show-employee/?id=<employeeId> e.g. /show-employee/?id=2
			server.createContext("/show-employee/", new GetOneEmployeeHandler());
			//use delete handler
			server.createContext("/delete/", new DeleteHandler());
			//use to process an update
			server.createContext("/process-update/", new UpdateDatabaseHandler());
			
			//Allows user to get all employees as JSON object
			server.createContext("/get-json", new GetJsonHandler());
			
			//Allows user to post JSON object into employee database
			server.createContext("/post-json", new PostJsonHandler());
			
			//Allows user to connect to home page of restful webservice.
			server.createContext("/", new GetHomeHandler());
			
			server.setExecutor(null); //default implementation of threading
			// start the server
			server.start();
			System.out.println("Server running on port 8014");
		}
		catch (IOException io) {
			System.out.println("Connection problem: "+io);
		}
	}
}