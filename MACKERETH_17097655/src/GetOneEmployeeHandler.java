import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;


/**
 * This class acts as a handler and allows the webservice to call the getEmployee method.
 * The employee object is retrieved from the database and displayed in the browser in JSON using GSON.
 * 
 @author Linton Mackereth
@version 1.0
@param String id
@return String json
@throws IOException

 */

public class GetOneEmployeeHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {

		Map<String, String> params = Utils.queryToMap(he.getRequestURI().getQuery());
		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
		EmployeeDAO dao = new EmployeeDAO();
		//Creates a new instance of GSON
		Gson gson = new Gson();

		//Creates a new Employee object with no values.
		Employee getEmployee = null;
		try {
			//Get's the employee by passing in string value of their ID.
			getEmployee = dao.getEmployee(params.get("id"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		//Creates a new String called JSON that converts the employee into JSON.
		String json = gson.toJson(getEmployee);
		
		//Returns a 200 ok.
		he.sendResponseHeaders(200, 0);
		//writes object out in JSON
		out.write(json);
		out.close();
	}

}