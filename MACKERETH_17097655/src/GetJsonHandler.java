import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
/**
 * This class implements a HttpHandler and calls the getAllEmployees method to retrieve
 * all employees from the SQlite database using the restful web service in JSON format.
 * 
@author Linton Mackereth - MMU ID 17097655
@version 1.0
@return String json
@Date 01/12/2017
@throws IOException

 */

public class GetJsonHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		
		// Creates a new BufferedWriter called out
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(he.getResponseBody()));
		
		//Creates a new ArrayList
		ArrayList<Employee> allEmployees = new ArrayList<>();
		//Created a new instance of EmployeeDAO called dao
		EmployeeDAO dao = new EmployeeDAO();
		//Creates new instance of GSON
		Gson gson = new Gson();

		try {
			//Calls the getAllEmployees method from EmployeeDAO class.
			allEmployees = dao.getAllEmployees();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		//Created a new string and converts values into JSON format.
		String json = gson.toJson(allEmployees);
		
		he.sendResponseHeaders(200, 0);
		out.write(json);
		out.close();
	}

}
