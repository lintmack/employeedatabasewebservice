
public class Employee extends Person {
	
	/**
	 * This class acts as a blue print to detail an Employee Object.
	 * 
	@author Linton Mackereth MMU ID 17097655
	@version 1.0
	@param employeeNumber, department, startDate, salary, email - calls(name, gender, dob, address, postcode) from Person Class
	@Date 01/12/2017

	 */

	//The employee class is the child class of Person.
	
	//Variables unique to the Employee Class.
	private int employeeNumber; 
	private String department; 
	private String startDate; 
	private float salary; 
	private String email;
	
	//Employee constructor details how an employee object is created.
	public Employee(String name, String gender, String dob, String address, String postcode, int employeeNumber,
			String department, String startDate, float salary, String email) {
		
		//Super calls the name, gender, dob, address and postcode from the Person class.
		super(name, gender, dob, address, postcode);
		
		this.employeeNumber = employeeNumber;
		this.department = department;
		this.startDate = startDate;
		this.salary = salary;
		this.email = email;
	}
	
	//Getters & Setters for Employee variables.
	public int getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	//String toString creates and returns a string from the employee variables.
	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + ", department=" + department + ", startDate=" + startDate
				+ ", salary=" + salary + ", email=" + email + "]";
	}
	
}